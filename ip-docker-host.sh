#!/bin/bash
ip addr show | grep docker0 | grep global | awk '{print $2}' | cut -d / -f1
