#!/bin/bash
if [ "$#" -eq 0 ]; then
  sudo docker run --rm --interactive --tty --publish 3333:3333 --volume /tmp/mydata:/opt/mydata --workdir /opt/mydata python:3
else
  if [ "$#" -eq 1 ]; then
    # i.e. instead of the python interpeter the command could be: /bin/bash
    sudo docker run --rm --interactive --tty --publish 3333:3333 --volume /tmp/mydata:/opt/mydata --workdir /opt/mydata python:3 "$1"
  else
    sudo docker run --rm --interactive --tty --publish "$2:$2" --volume /tmp/mydata:/opt/mydata --workdir /opt/mydata python:3 "$1"
  fi
fi
