# https://github.com/kylemanna/docker-openvpn
# https://openvpn.net/index.php/open-source/documentation/howto.html
export FQDN="physicstime.com"
export OVPN_DATA="ovpn-data"
docker volume create --name $OVPN_DATA
# generate the initial configuration in the volume
docker run -v $OVPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_genconfig -u udp://$FQDN
# generate the certificate in the volume (you must choose a passphrase)
docker run -v $OVPN_DATA:/etc/openvpn --rm -it kylemanna/openvpn ovpn_initpki
# start the openvpn service
docker run -v $OVPN_DATA:/etc/openvpn -d -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn

# generate the client certificate without the passphrase
docker run -v $OVPN_DATA:/etc/openvpn --rm -it kylemanna/openvpn easyrsa build-client-full $FQDN nopass
# export the client config with embedded certificates
docker run -v $OVPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_getclient $FQDN > $FQDN.ovpn

# From a client computer SCP to download the $FQDN>ovpn and then connect to the openvpn server
# sudo apt-get install -y openvpn
# sudo openvpn --config $FQDN.ovpn
# verify with the following in the output: "/sbin/ip addr add dev tun0 local 192.168.255.6 peer 192.168.255.5"
# ifconfig -a  "tun0 ... inet addr:192.168.255.6"  , as you send traffic: RX bytes:4145707 (4.1 MB)  TX bytes:319025 (319.0 KB)
# curl checkip.amazonaws.com  , should return the IP address of the VPN server (not your local Wifi/ISP)
# curl https://dnsleaktest.com/
# https://whoer.net/#extended

