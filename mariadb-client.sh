#!/bin/bash

# TODO: add a bash parameter or ENV override to target a specific container name or id for the link
# TODO: use an ENV variable to customize the mysql user and password

if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

# this is required if the mariadb mysql server is running with 127.0.0.1:3306:3306 (only the host can bind to 3306)
# assumes only a single mariadb server running on this host
MARIADB_CONTAINER=`docker ps -a | grep mariadb | cut -d" " -f1`
sudo docker run --rm -i -t --link $MARIADB_CONTAINER:mysql mariadb:5 /bin/bash -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -uroot -p'
# docker run --rm -i -t --link myhipchat_mariadb_1:mysql mariadb:5 /bin/bash -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p'

# HOSTIP=`ip addr show | grep docker0 | grep global | awk '{print $2}' | cut -d / -f1`
# sudo docker run --rm -i -t mariadb:5 mysql -h"$HOSTIP" -uroot -p'
