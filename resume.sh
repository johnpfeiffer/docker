#!/bin/bash
if [ $# -ne 1 ]; then
  sudo docker ps -a
fi
sudo docker start --interactive --attach $1
