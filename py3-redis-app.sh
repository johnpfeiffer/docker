#!/bin/bash
if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

./redis-start.sh

STATUS=$(docker ps -a | grep py3_redis_app)
if [ -z "$STATUS" ]; then
  docker run --detach --publish 5000:5000 --publish 127.0.0.1:2223:22 --volume /opt/mydata:/opt/mydata --link redis:db --name py3_redis_app python3-ssh
else
  docker start py3_redis_app
fi
docker ps | grep py3_redis_app
REDISHOST=$(./ip-docker-container.sh redis)
if [ -z "$REDISHOST" ]; then
  echo "ERROR: unable to find redis host ip address"
else
  echo "$REDISHOST" > /opt/mydata/redis.config
fi
