#!/bin/bash
if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

STATUS=$(docker ps -a | grep mariadb)
if [ -z "$STATUS" ]; then
  echo "starting..."
  sudo docker run -d --name mariadb -e MYSQL_ROOT_PASSWORD=mypassword -e MYSQL_DATABASE=mydatabase -p 3306:3306 mariadb:5
else
  echo "re-attaching..."
  docker start mariadb 
fi
docker ps | grep mariadb 


