#!/bin/bash
docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}') --env ADVERTISED_PORT=9092 spotify/kafka

