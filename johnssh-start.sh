#!/bin/bash
if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

STATUS=$(docker ps -a | grep johnssh)
if [ -z "$STATUS" ]; then
  docker run --detach --publish 127.0.0.1:2222:22 --volume /opt/mydata:/opt/mydata --name johnssh trustyssh
else
  docker start johnssh 
fi
docker ps | grep johnssh
