#!/bin/bash
if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

STATUS=$(docker ps -a | grep redis)
if [ -z "$STATUS" ]; then
  docker run --detach --name redis redis
else
  docker start redis
fi
docker ps | grep redis
