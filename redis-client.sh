#!/bin/bash
# TODO: add a bash parameter or ENV override to target a specific container name or id for the link
# TODO: add a bash parameter or ENV override for the target connection port
if [ "$(whoami)" != "root" ]; then
  echo "ERROR: requires sudo or root."
  exit 1
fi

# this is required if the container server is running with 127.0.0.1:6379:6379 (bound so only the host can connect)
# assumes only a single server running on this host
SERVER_CONTAINER=`docker ps -a | grep redis | cut -d" " -f1`
echo $SERVER_CONTAINER
docker run --rm -i -t --link $SERVER_CONTAINER:myredis redis redis-cli -h myredis -p 6379

# assumes the server port allows all access, i.e. 6379:6379
# HOSTIP=`ip addr show | grep docker0 | grep global | awk '{print $2}' | cut -d / -f1`
# docker run --rm -i -t redis redis-cli -h $HOSTIP -p 6379
