#!/bin/bash
# https://hub.docker.com/r/spotify/kafka/
# http://kafka.apache.org/documentation.html
docker run --rm -it --env ADVERTISED_HOST=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}') --env ADVERTISED_PORT=9092 spotify/kafka /bin/bash
# $KAFKA_HOME/bin/kafka-topics.sh --zookeeper  $ADVERTISED_HOST:2181 --list
# $KAFKA_HOME/bin/kafka-topics.sh --zookeeper  $ADVERTISED_HOST:2181 --create --topic mytest --partitions 1 --replication-factor 1

# interactive producer of messages for the topic
# $KAFKA_HOME/bin/kafka-console-producer.sh --broker-list $ADVERTISED_HOST:9092 --topic mytest

